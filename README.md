# Introduction

chaseR is an R package to compute correlations in chromatin networks.
Its usage is described in this paper: 
https://doi.org/10.1101/717298.

# Installation


```R
library(devtools)
devtools::install_bitbucket("eraineri/chaser",  build_vignettes=TRUE)
```



To read the vignette:

```R
browseVignettes(package="chaser")
```

## RCy3

ChAseR relies on the RCy3 package to implement the method `plot` which sends 
chromnet objects to Cytoscape (https://cytoscape.org/) to represent them graphically.
We use the development version of `RCy3` that is available through 
github (https://github.com/cytoscape/RCy3).
Users should install it manually if they want to use `plot`.



